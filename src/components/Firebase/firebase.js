import app from 'firebase/app';
import 'firebase/auth';

const firebaseConfig = {
    apiKey: "AIzaSyDGC4d6dUYX9pXbWsF5KJ3uAU3kDUCDTXk",
    authDomain: "react-starter-project-qg70.firebaseapp.com",
    databaseURL: "https://react-starter-project-qg70.firebaseio.com",
    projectId: "react-starter-project-qg70",
    storageBucket: "react-starter-project-qg70.appspot.com",
    messagingSenderId: "302860519712",
    appId: "1:302860519712:web:cb50833086afde5df86f45",
    measurementId: "G-HVPG048E4J"
  };

  class Firebase {
    constructor() {
      app.initializeApp(firebaseConfig);

      this.auth = app.auth();
    }

    doCreateUserWithEmailAndPassword = (email, password) =>
    this.auth.createUserWithEmailAndPassword(email, password);

    doSignInWithEmailAndPassword = (email, password) => this.auth.signInWithEmailAndPassword(email, password);

    doSignOut = () => this.auth.signOut();

    doPasswordReset = email => this.auth.sendPasswordResetEmail(email);

    doPasswordUpdate = password => this.auth.currentUser.updatePassword(password);
}
  export default Firebase;